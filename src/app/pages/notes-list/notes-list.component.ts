import {AfterViewInit, Component, ElementRef, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {BehaviorSubject, fromEvent, Observable, Subject} from "rxjs";
import {map, startWith, debounceTime, distinctUntilChanged, takeUntil, skipWhile} from 'rxjs/operators';

import {Note} from "../../shared/note.model";
import {NotesService} from "../../shared/notes.service";
import {notesAnimation} from "../../shared/animation";

@Component({
  selector: 'app-notes-list',
  templateUrl: './notes-list.component.html',
  styleUrls: ['./notes-list.component.scss'],
  animations: notesAnimation
})
export class NotesListComponent implements OnInit, AfterViewInit, OnDestroy {
  @ViewChild('search') search: ElementRef;

  public notes: BehaviorSubject<Note[]>;
  public doneNotes: BehaviorSubject<Note[]>;

  private unsubscribe = new Subject<void>();

  constructor(private notesService: NotesService) {
  }

  ngOnInit() {
    this.notes = this.notesService.notes;
    this.doneNotes = this.notesService.done;
  }

  ngOnDestroy(): void {
    this.unsubscribe.next();
    this.unsubscribe.complete();
  }

  ngAfterViewInit(): void {
    fromEvent<any>(this.search.nativeElement, 'keyup')
      .pipe(
        map(event => event.target.value),
        startWith(''),
        debounceTime(400),
        distinctUntilChanged(),
        skipWhile((value => value.length < 3)),
        takeUntil(this.unsubscribe)
      ).subscribe({
      next: (inputValue) => {
        this.notesService.search(inputValue).subscribe({
          next: () => console.log('Search'),
          error: () => console.log('[TODO] Create some notification about error')
        });
      }
    });
  }

  public deleteNote(id: string): void {
    this.notesService.delete(id).subscribe({
      next: () => console.log('Note deleted!'),
      error: () => console.log('[TODO] Create some notification about error')
    });
  }
}
