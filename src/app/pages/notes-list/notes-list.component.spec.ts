import {ComponentFixture, TestBed} from "@angular/core/testing";
import {NotesListComponent} from "./notes-list.component";
import { MockComponent } from 'ng-mocks';
import {NoteCardComponent} from "../../note-card/note-card.component";
import {NotesService} from "../../shared/notes.service";
import {BehaviorSubject} from "rxjs";
import {Note} from "../../shared/note.model";


describe('Notes-List Component', () => {
  let fixture: ComponentFixture<NotesListComponent>;
  let component: NotesListComponent;
  const mockNotesService = {} as NotesService;

  beforeEach(() => {
    mockNotesService.search = () => null;
    mockNotesService.delete = () => null;
    mockNotesService.notes = new BehaviorSubject<Note[]>([]);
    mockNotesService.done = new BehaviorSubject<Note[]>([]);
  });

  beforeEach(() => {
    spyOn(mockNotesService, 'delete');
    spyOn(mockNotesService, 'search');
  });

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [],
      declarations: [
        NotesListComponent,
        MockComponent(NoteCardComponent)
      ],
      providers: [{
        provide: NotesService, useValue: mockNotesService
      }]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NotesListComponent);
    component = fixture.debugElement.componentInstance;
  });

  it('Component should be defined', () => {
    expect(component).toBeDefined();
  });
});
