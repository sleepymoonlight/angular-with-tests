import {ComponentFixture, TestBed} from "@angular/core/testing";
import {ArchivedPageComponent} from "./archived-page.component";
import {MockComponent} from 'ng-mocks';
import {NoteCardComponent} from "../../note-card/note-card.component";
import {NotesService} from "../../shared/notes.service";
import {BehaviorSubject} from "rxjs";
import {Note} from "../../shared/note.model";

describe('Archived-Page Component', () => {
  let fixture: ComponentFixture<ArchivedPageComponent>;
  let component: ArchivedPageComponent;
  const mockNotesService = {} as NotesService;

  beforeEach(() => {
    mockNotesService.delete = () => null;
    mockNotesService.archived = new BehaviorSubject<Note[]>([]);
  });


  beforeEach(() => {
    spyOn(mockNotesService, 'delete');
  });

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [],
      declarations: [
        ArchivedPageComponent,
        MockComponent(NoteCardComponent)
      ],
      providers: [{
        provide: NotesService, useValue: mockNotesService
      }]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ArchivedPageComponent);
    component = fixture.debugElement.componentInstance;
  });

  it('Component should be defined', () => {
    expect(component).toBeDefined();
  });

});
