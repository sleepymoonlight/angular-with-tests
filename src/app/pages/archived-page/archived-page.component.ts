import {Component, OnDestroy, OnInit} from '@angular/core';
import {BehaviorSubject, Subject} from "rxjs";

import {Note} from "../../shared/note.model";
import {NotesService} from "../../shared/notes.service";
import {notesAnimation} from "../../shared/animation";

@Component({
  selector: 'app-archived-page',
  templateUrl: './archived-page.component.html',
  styleUrls: ['./archived-page.component.scss'],
  animations: notesAnimation
})
export class ArchivedPageComponent implements OnInit, OnDestroy {
  public notes: BehaviorSubject<Note[]>;

  private unsubscribe = new Subject<void>();

  constructor(private notesService: NotesService) {
  }

  ngOnInit() {
    this.notes = this.notesService.archived;
  }

  ngOnDestroy(): void {
    this.unsubscribe.next();
    this.unsubscribe.complete();
  }

  public deleteNote(id: string): void {
    this.notesService.delete(id).subscribe({
      next: () => console.log('Note deleted!'),
      error: () => console.log('[TODO] Create some notification about error')
    });
  }
}
