import {Component, OnInit} from '@angular/core';
import {NgForm} from "@angular/forms";
import {Note} from "../../shared/note.model";
import {NotesService} from "../../shared/notes.service";
import {ActivatedRoute, Params, Router} from "@angular/router";
import {Observable, of} from "rxjs";

@Component({
  selector: 'app-note-details',
  templateUrl: './note-details.component.html',
  styleUrls: ['./note-details.component.scss']
})
export class NoteDetailsComponent implements OnInit {

  public note: Observable<Note>;
  public noteId: string;
  public isNewNote: boolean;

  constructor(private notesService: NotesService, private router: Router, private route: ActivatedRoute) {
  }

  ngOnInit(): void {
    this.route.params.subscribe((params: Params) => {
      this.note = of(new Note());

      if (params.id) {
        this.note = this.notesService.getById(params.id);
        this.noteId = params.id;
        this.isNewNote = false;
      } else {
        this.isNewNote = true;
      }
    });
  }

  onSubmit(form: NgForm): void {
    if (this.isNewNote) {
      this.notesService.add({
        title: form.value.title,
        details: form.value.details,
        isDone: false,
        id: this.generateId(),
        isArchived: false
      })
        .subscribe({
          next: () => console.log('Note has been added.'),
          error: () => console.log('Can`t add new note.')
        });
    } else {
      this.notesService.update(this.noteId, {title: form.value.title, details: form.value.details})
        .subscribe({
          next: () => console.log('Note updated.'),
          error: () => console.log('Can`t update the note.')
        });
    }

    this.router.navigateByUrl('/');
  }

  onCancel(): void {
    this.router.navigateByUrl('/');
  }

  private generateId(): string {
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, (c) => {
      const r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
      return v.toString(16);
    });
  }
}
