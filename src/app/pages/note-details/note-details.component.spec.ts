import {ComponentFixture, TestBed} from "@angular/core/testing";
import {NoteDetailsComponent} from "./note-details.component";
import {FormsModule, NgForm} from "@angular/forms";
import {RouterTestingModule} from "@angular/router/testing";
import {NotesService} from "../../shared/notes.service";
import {ActivatedRoute, Router} from "@angular/router";
import {of} from "rxjs";

describe('Note-Details Component', () => {
  let fixture: ComponentFixture<NoteDetailsComponent>;
  let component: NoteDetailsComponent;
  const mockNotesService = {} as NotesService;
  const mockRouteService = {} as ActivatedRoute;
  describe('Edit Notes', () => {
    beforeEach(() => {
      mockRouteService.params = of({id: 'test'});
      mockNotesService.getById = () => null;
      mockNotesService.add = () => of({});
      mockNotesService.update = () => null;
    });

    beforeEach(() => {
      spyOn(mockNotesService, 'getById').and.returnValue(of({}));
      spyOn(mockNotesService, 'add').and.returnValue(of({}));
      spyOn(mockNotesService, 'update').and.returnValue(of({}));
    });

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [
          FormsModule,
          RouterTestingModule
        ],
        declarations: [NoteDetailsComponent],
        providers: [{
          provide: NotesService, useValue: mockNotesService
        }, {
          provide: ActivatedRoute, useValue: mockRouteService
        }]
      }).compileComponents();
    });

    beforeEach(() => {
      fixture = TestBed.createComponent(NoteDetailsComponent);
      component = fixture.debugElement.componentInstance;
    });

    it('Component should be defined', () => {
      expect(component).toBeDefined();
    });

    it('Should init notes', async () => {
      expect(component.note).toBeUndefined();
      expect(component.noteId).toBeUndefined();
      expect(component.isNewNote).toBeUndefined();

      fixture.detectChanges();

      expect(component.note).toBeDefined();
      expect(component.noteId).toBeDefined();
      expect(component.isNewNote).toBeFalsy();
    });

    it('', () => {
      fixture.detectChanges();
      component.onSubmit({
        value: {
          title: 'Test'
        }
      } as NgForm);
      expect(mockNotesService.update).toHaveBeenCalled();
    });
  });

  describe('Edit', () => {
    beforeEach(() => {
      mockRouteService.params = of({id: undefined});
      mockNotesService.getById = () => of(null);
      mockNotesService.add = () => of({});
      mockNotesService.update = () => of({});
    });

    beforeEach(() => {
      spyOn(mockNotesService, 'getById').and.returnValue(of({}));
      spyOn(mockNotesService, 'add').and.returnValue(of({}));
      spyOn(mockNotesService, 'update').and.returnValue(of({}));
    });

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [
          FormsModule,
          RouterTestingModule
        ],
        declarations: [NoteDetailsComponent],
        providers: [{
          provide: NotesService, useValue: mockNotesService
        }, {
          provide: ActivatedRoute, useValue: mockRouteService
        }]
      }).compileComponents();
    });

    beforeEach(() => {
      fixture = TestBed.createComponent(NoteDetailsComponent);
      component = fixture.debugElement.componentInstance;
    });
    it('Should init notes', async () => {

      expect(component.note).toBeUndefined();
      expect(component.noteId).toBeUndefined();
      expect(component.isNewNote).toBeUndefined();

      fixture.detectChanges();

      expect(component.note).toBeDefined();
      expect(component.noteId).toBeUndefined();
      expect(component.isNewNote).toBeTruthy();
    });

    it('Add on submit', () => {
      fixture.detectChanges();
      component.onSubmit({
        value: {
          title: 'Test'
        }
      } as NgForm);
      expect(mockNotesService.add).toHaveBeenCalled();
    })
  });


});
