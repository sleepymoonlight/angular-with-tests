import {ComponentFixture, TestBed} from "@angular/core/testing";
import {MainLayoutComponent} from "./main-layout.component";
import {RouterTestingModule} from "@angular/router/testing";

describe('Main-Layout Component', () => {
  let fixture: ComponentFixture<MainLayoutComponent>;
  let component: MainLayoutComponent;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule
      ],
      declarations: [MainLayoutComponent],
      providers: []
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MainLayoutComponent);
    component = fixture.debugElement.componentInstance;
  });

  it('Component should be defined', () => {
    expect(component).toBeDefined();
  });

});
