import {Component, ElementRef, OnInit, ViewChild, Renderer2, Input, Output, EventEmitter} from '@angular/core';
import {NotesService} from "../shared/notes.service";

@Component({
  selector: 'app-note-card',
  templateUrl: './note-card.component.html',
  styleUrls: ['./note-card.component.scss']
})
export class NoteCardComponent implements OnInit {

  @Input() title: string;
  @Input() details: string;
  @Input() link: string;
  @Input() isDone: boolean;
  @Input() isArchived: boolean;
  @Input() id: string;

  @Output('delete') deleteEvent: EventEmitter<void> = new EventEmitter<void>();

  @ViewChild('truncator') truncator: ElementRef<HTMLElement>;
  @ViewChild('bodyText') bodyText: ElementRef<HTMLElement>;

  constructor(private renderer: Renderer2, private notesService: NotesService) { }

  ngOnInit() {
    const style = window.getComputedStyle(this.bodyText.nativeElement, null);
    const viewableHeight = parseInt(style.getPropertyValue('height'), 10);

    if (this.bodyText.nativeElement.scrollHeight > viewableHeight) {
        this.renderer.setStyle(this.truncator.nativeElement, 'display', 'block');
    } else {
      this.renderer.setStyle(this.truncator.nativeElement, 'display', 'none');
    }
  }

  onXButtonClick() {
    this.deleteEvent.emit();
  }

  changeStatus(event) {
    this.isDone = event.target.checked;
    this.notesService.update(this.id, {isDone: this.isDone})
      .subscribe({
        next: () => console.log('Note updated.'),
        error: () => console.log('Can`t update the note.')
      });
  }

  archive() {
    this.isArchived = !this.isArchived;
    this.notesService.update(this.id, {isArchived: this.isArchived})
      .subscribe({
        next: () => console.log('Note updated.'),
        error: () => console.log('Can`t update the note.')
      });
  }
}
