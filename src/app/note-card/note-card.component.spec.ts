import {ComponentFixture, TestBed} from "@angular/core/testing";
import {NoteCardComponent} from "./note-card.component";
import {FormsModule} from "@angular/forms";
import {RouterTestingModule} from "@angular/router/testing";
import {NotesService} from "../shared/notes.service";
import {of} from "rxjs";

describe('Note-Card Component', () => {
  let fixture: ComponentFixture<NoteCardComponent>;
  let component: NoteCardComponent;
  const mockNotesService = {} as NotesService;

  beforeEach(() => {
    mockNotesService.update = () => null;
  });


  beforeEach(() => {
    spyOn(mockNotesService, 'update').and.returnValue(of({}));
  });


  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        FormsModule,
        RouterTestingModule
      ],
      declarations: [NoteCardComponent],
      providers: [{
        provide: NotesService, useValue: mockNotesService
      }]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NoteCardComponent);
    component = fixture.debugElement.componentInstance;
  });

  it('Component should be defined', () => {
    expect(component).toBeDefined();
  });

  it('Should change status', () => {
    const event = {
      target: {checked: true}
    };

    component.changeStatus(event);

    expect(mockNotesService.update).toHaveBeenCalled();
  });

  it('Should archive note', () => {
    component.isArchived = false;
    component.archive();
    expect(component.isArchived).toBeTruthy();
    expect(mockNotesService.update).toHaveBeenCalled();
  })
});
