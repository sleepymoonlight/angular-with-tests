import {Injectable} from '@angular/core';
import {Note} from './note.model';
import {HttpClient, HttpParams} from "@angular/common/http";
import {BehaviorSubject, Observable} from "rxjs";
import {tap} from "rxjs/operators";

@Injectable({
  providedIn: 'root'
})
export class NotesService {
  public notes = new BehaviorSubject<Note[]>([]);
  public archived = new BehaviorSubject<Note[]>([]);
  public done = new BehaviorSubject<Note[]>([]);

  constructor(private http: HttpClient) {
    this.fetchNotes();
    this.getDone();
    this.getArchived();
  }

  public fetchNotes(): void {
    this.http.get<Note[]>('http://localhost:3000/todos?isDone=false&isArchived=false')
      .pipe(tap(notes => this.notes.next((notes))))
      .subscribe();
  }

  public getDone(): void {
    this.http.get<Note[]>(`http://localhost:3000/todos?isDone=true&isArchived=false`)
      .pipe(tap(notes => this.done.next((notes))))
      .subscribe();
  }

  public getArchived(): void {
    this.http.get<Note[]>(`http://localhost:3000/todos?isArchived=true`)
      .pipe(tap(notes => this.archived.next((notes))))
      .subscribe();
  }

  public getById(id: string): Observable<Note> {
    return this.http.get<Note>(`http://localhost:3000/todos/${id}`);
  }

  public search(subString: string): Observable<Note[]> {
    const params = new HttpParams()
      .append('q', subString);

    return this.http.get<Note[]>('http://localhost:3000/todos?isDone=false&isArchived=false', {params})
      .pipe(tap((notes) => {
        this.notes.next(notes);
      }));
  }

  public add(note: Note) {
    return this.http.post('http://localhost:3000/todos', note)
      .pipe(tap(() => this.fetchNotes()));
  }

  public update(id: string, note: Partial<Note>) {
    return this.http.patch(`http://localhost:3000/todos/${id}`, note)
      .pipe(tap(() => {
        this.fetchNotes();
        this.getDone();
        this.getArchived();
      }));
  }

  public delete(id: string) {
    return this.http.delete(`http://localhost:3000/todos/${id}`)
      .pipe(tap(() => {
        this.fetchNotes();
        this.getDone();
        this.getArchived();
      }));
  }
}
