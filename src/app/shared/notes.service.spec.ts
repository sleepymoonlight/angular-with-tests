import {NotesService} from "./notes.service";
import {HttpClientTestingModule, HttpTestingController, TestRequest} from "@angular/common/http/testing";
import {TestBed} from "@angular/core/testing";
import {skipWhile} from "rxjs/operators";

describe('Notes Service', () => {
  let service: NotesService;
  let http: HttpTestingController;

  const mockNotes = [
    {
      "title": "asdasd",
      "details": "asdasdasd",
      "isDone": false,
      "id": "7430e087-d2d1-46e8-bf18-93c2d8d66235",
      "isArchived": false
    },
    {
      "title": "1111",
      "details": "11111",
      "isDone": true,
      "id": "46205af2-0c63-41cf-9f2f-eb9bada24474",
      "isArchived": false
    }
  ];

  beforeEach(() => {
    TestBed.configureTestingModule({
        imports: [
          HttpClientTestingModule
        ],
        providers: [
          NotesService
        ]
      }
    );
  });

  beforeEach(() => {
    service = TestBed.get(NotesService);
    http = TestBed.get(HttpTestingController);
  });

  it('Should fetchNotes on init and return notes', async () => {
    let fetchRequest: TestRequest;

    service.notes
      .pipe(
        skipWhile((notes) => !!notes)
      ).subscribe((res) => {
      expect(res).toEqual(mockNotes);
    });

    expect(() => {
      fetchRequest = http.expectOne('http://localhost:3000/todos?isDone=false&isArchived=false');
    }).not.toThrow();

    fetchRequest.flush(mockNotes);
  });

  it('Should getDone on init', async () => {
    let getDone: TestRequest;

    service.done
      .pipe(
        skipWhile((notes) => !!notes)
      ).subscribe((res) => {
      expect(res).toEqual(mockNotes);
    });

    expect(() => {
      getDone = http.expectOne('http://localhost:3000/todos?isDone=true&isArchived=false');
    }).not.toThrow();

    getDone.flush(mockNotes);
  });

  it('Should getArchived on init', async () => {
    let getArchived: TestRequest;

    service.archived
      .pipe(
        skipWhile((notes) => !!notes)
      ).subscribe((res) => {
      expect(res).toEqual(mockNotes);
    });

    expect(() => {
      getArchived = http.expectOne('http://localhost:3000/todos?isArchived=true');
    }).not.toThrow();

    getArchived.flush(mockNotes);
  });

  it('should call getById', () => {
    let getById: TestRequest;

    service.getById('123')
      .pipe(
        skipWhile((notes) => !!notes)
      ).subscribe((res) => {
      expect(res).toEqual(mockNotes[0]);
    });

    expect(() => {
      getById = http.expectOne(`http://localhost:3000/todos/${123}`);
    }).not.toThrow();

    getById.flush(mockNotes[0]);
  });

  it('should search in notes', () => {
    let search: TestRequest;

    service.search('333')
      .pipe(
        skipWhile((notes) => !!notes)
      ).subscribe((res) => {
      expect(res).toEqual(mockNotes);
    });

    service.notes
      .pipe(
        skipWhile((notes) => !!notes)
      ).subscribe((res) => {
      expect(res).toEqual(mockNotes);
    });

    expect(() => {
      search = http.expectOne('http://localhost:3000/todos?isDone=false&isArchived=false&q=333');
    }).not.toThrow();

    search.flush(mockNotes);
  });

  it('should add note', () => {
    let add: TestRequest;

    service.add(mockNotes[0])
      .pipe(
        skipWhile((notes) => !!notes)
      ).subscribe((res) => {
      expect(res).toEqual(mockNotes);
    });

    service.notes
      .pipe(
        skipWhile((notes) => !!notes)
      ).subscribe((res) => {
      expect(res).toEqual(mockNotes);
    });

    expect(() => {
      add = http.expectOne(`http://localhost:3000/todos`);
    }).not.toThrow();

    add.flush(mockNotes);
  });

  it('should update note', () => {
    let update: TestRequest;

    service.update('7430e087-d2d1-46e8-bf18-93c2d8d66235', mockNotes[0])
      .pipe(
        skipWhile((notes) => !!notes)
      ).subscribe((res) => {
      expect(res).toEqual(mockNotes);
    });

    service.notes
      .pipe(
        skipWhile((notes) => !!notes)
      ).subscribe((res) => {
      expect(res).toEqual(mockNotes);
    });

    expect(() => {
      update = http.expectOne(`http://localhost:3000/todos/7430e087-d2d1-46e8-bf18-93c2d8d66235`);
    }).not.toThrow();

    update.flush(mockNotes);
  });

  it('should delete note', () => {
    let deleteNote: TestRequest;

    service.delete('7430e087-d2d1-46e8-bf18-93c2d8d66235')
      .pipe(
        skipWhile((notes) => !!notes)
      ).subscribe((res) => {
      expect(res).toEqual(mockNotes);
    });

    service.notes
      .pipe(
        skipWhile((notes) => !!notes)
      ).subscribe((res) => {
      expect(res).toEqual(mockNotes);
    });

    expect(() => {
      deleteNote = http.expectOne(`http://localhost:3000/todos/7430e087-d2d1-46e8-bf18-93c2d8d66235`);
    }).not.toThrow();

    deleteNote.flush(mockNotes);
  });
});
