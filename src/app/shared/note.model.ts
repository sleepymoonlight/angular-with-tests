export class Note {
  public title: string;
  public details: string;
  public id: string;
  public isDone: boolean;
  public isArchived: boolean;
}
